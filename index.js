const http = require('http');
const router = require('./routes/router');
const logger = require('./utils/logger');

const port = process.env.PORT || 3000;

const requestListener = (req, res) => {
  res.setHeader('Content-Type', 'application/json');

  router(req)
    .then((data) => {
      res.write(JSON.stringify(data));
      logger.logRequest(req, res);
    })
    .catch((e) => {
      res.statusCode = e.statusCode || 500;
      res.write(JSON.stringify({ error: e.message }));
      logger.logRequest(req, res, e.message);
    })
    .finally(() => {
      res.end();
    });
}

const server = http.createServer(requestListener);

server.listen(port, (err) => {
  if (err) {
    return console.log('Something went wrong', err);
  }

  console.log(`Server is listening on ${port}`);
});