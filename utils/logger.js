const fs = require('fs');

function info(msg) {
  fs.appendFileSync('request.log', `${msg}\n`);
}

function logRequest(req, res, error = '') {
  const date = getDate();
  let msg = `${date} | ${req.method} ${req.url} | ${res.statusCode}`;
  if (!!error) {
    msg += ` - ${error}`;
  }

  info(msg);
}

function getDate() {
  const now = new Date();
  return `${toDouble(now.getMonth() + 1)}/${toDouble(now.getDate())}/${toDouble(now.getFullYear(), '')} ${toDouble(now.getHours())}:${toDouble(now.getMinutes())}:${toDouble(now.getSeconds())}`;
}

const toDouble = (number, symbol = '0') => (symbol + number).slice(-2);

module.exports = { logRequest };