const stockService = require('../service/stock.service');
const createError = require('../service/api.service').createError;


async function router(req) {
  if (req.method !== 'GET') {
    throw createError('Not found', 404);
  }

  const pathParts = req.url.split('/').filter(part => part);
  switch (pathParts[0]) {
    case 'stock':
      if (pathParts.length === 2) {
        return stockService.getCompanyInfo(pathParts[1]);
      }
    default:
      throw createError('Not found', 404);
  }
}

module.exports = router;