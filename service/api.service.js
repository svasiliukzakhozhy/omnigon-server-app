const https = require('https');


const baseURL = 'https://api.iextrading.com/1.0';

function createError(message, statusCode) {
  const error = new Error(message);
  error.statusCode = statusCode || 500;

  return error;
}

function get(url) {
  return new Promise((resolve, reject) => {
    https.get(`${baseURL}${url}`, (res) => {
      const { statusCode } = res;

      if (statusCode !== 200) {
        reject(createError(`Third-party service respond with ${statusCode}`, statusCode));

        return;
      }

      let buffer = '';
      res.on('data', (chunk) => {
        buffer += chunk;
      });
      res.on('end', () => {
        try {
          const data = JSON.parse(buffer);
          resolve(data);
        } catch (e) {
          reject(createError(`Third-party service respond with non-json`));
        }
      });
    }).on('error', (e) => {
      reject(createError(`Request to third-party service was failed`));
    });
  })
}

module.exports = { get, createError };