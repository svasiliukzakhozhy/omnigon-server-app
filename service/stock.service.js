const apiService = require('./api.service');


async function getPrice(ticker) {
  const res = await apiService.get(`/stock/${ticker}/quote`);

  return res.latestPrice;
}

async function getLogoUrl(ticker) {
  const res = await apiService.get(`/stock/${ticker}/logo`);

  return res.url;
}

async function getLinkToLatestNews(ticker, count = 1) {
  const res = await apiService.get(`/stock/${ticker}/news/last/${count}`);

  if (!res.length) {
    return null;
  }

  return res[0].url;
}

async function getCompanyInfo(ticker) {
  const [price, logoUrl, linkToLatestNews] = await Promise.all([
    getPrice(ticker),
    getLogoUrl(ticker),
    getLinkToLatestNews(ticker),
  ]);

  return { price, logoUrl, linkToLatestNews };
}

module.exports = { getCompanyInfo }