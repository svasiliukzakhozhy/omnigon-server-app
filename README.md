# Stock ticker backend application

## Features

Server exposes an HTTP REST API endpoint that takes a stock ticker symbol and returns a json object containing:

* The latest stock price
* A path to the company logo
* Link to a latest news article for the company

Also, the program logs the incoming request to a file and track:

* The request URL path
* The time of the request in the format of MM/DD/YY HH:mm:ss
* If the request was fulfilled successfully or if it failed to respond to the requestor.

## Running the server

### Clone repository

```sh
$ git clone https://bitbucket.org/svasiliukzakhozhy/omnigon-server-app.git
$ cd omnigon-server-app
```

### Start the development server

```sh
$ npm start
... Server is listening on 3000
```

### Examples
```
GET: http://localhost:3000/stock/aapl
{
    "price": 203.13,
    "logoUrl": "https://storage.googleapis.com/iex/api/logos/AAPL.png",
    "linkToLatestNews": "https://api.iextrading.com/1.0/stock/aapl/article/5099381974575993"
}
```